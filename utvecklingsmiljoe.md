---
description: Installera alla verktyg, konfigurera alla inställningar, skapar alla mappar
---

# Utvecklingsmiljö

## Installera webbeditorn VS Code & tillägg

* Installera [VS Code](https://code.visualstudio.com)
* Installera tilläggen
  * Beautify
  * Live Server
  * Path Intellisense
* Installera [git-scm](https://git-scm.com)
