# Använda text

## Text och strängar

[https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Strings](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/First_steps/Strings)

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Introduktion i Javascript</title>
</head>
<body>

    <script>
        // Skapar en variabel
        var ålder;

        // Fyller variabeln med svaret
        ålder = prompt("Hur gammal är du?");

        // Skriv ut svaret i loggen
        console.log(ålder);

        // Lite roligare utskrift
        console.log("Din ålder är " + ålder);

        // Skriv ut: Hej ..., din ålder är ...
        console.log("Hej " + namn + ", din ålder är " + ålder + " år.");

        // Alternativ 2, på en ny rad: \n
        console.log("Hej " + namn + ".\nDin ålder är " + ålder + " år.");
    </script>
</body>
</html>
```

## Strängsubstitution

Ett nyare sätt att sätta samman text är strängsubstitution istället för att använda '**+**'. Man använder då sk backticks '**\`**', se exemplet nedan:

```php
<script>
    var namn = "John";

    // Bädda in en variabel
    alert(`Hur mår du ${name}?`); // Hur mår du John?

    // Bädda in ett uttryck
    alert(`Resultatet blir ${1 + 2}`); // Resultatet blir 3
</script>
```
