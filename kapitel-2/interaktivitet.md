---
description: Det blir roligare när användaren får styra sidan
---

# Interaktivitet

## Köra kod från webbsidan

### onclick

{% tabs %}
{% tab title="JavaScript" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Göra webbsidan klickbar</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1>Lyssna på knapphändelser</h1>
        <button onclick="varning()">Klicka inte på mig!</button>
    </div>
    
    <script>
        function varning() {
            alert("Du är virussmittad!");
        }
    </script>
</body>
</html>
```
{% endtab %}

{% tab title="style.css" %}
```css
@import url('https://fonts.googleapis.com/css?family=Open+Sans');

/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
body {
    background: #F9F6EB;
}
.kontainer {
    width: 600px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Open Sans', sans-serif;
    border: 1px solid #ddd;
    box-shadow: 0 0 12px #f0e9d1;
}
.kontainer #myCanvas {
    border: 1px solid;
}
.kol2 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr;
    grid-gap: 1em;
}
.kol3 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr 1fr;
    grid-gap: 1em;
}
.kol8 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: auto auto auto auto auto auto auto auto;
    grid-gap: 1em;
}
.kontainer label {
    text-align: right;
    align-self: center;
    font-size: 0.9em;
}
.kontainer input, .kontainer textarea {
    padding: 0.7em;
    border-radius: 0.3em;
    border: 1px solid #ccc;
    font-weight: bold;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
.kontainer textarea {
    height: 5em;
    width: 100%;
}
.kontainer button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
    cursor: pointer;
}
.kontainer h1, .kontainer h2, .kontainer h3 {
    margin: 0.5em 0;
}
.kontainer p {
    background: #FFF;
    padding: 5px;
    border-radius: 3px;
    margin-top: 0;
    margin-bottom: 0;
    font-size: 14px;
}
.kontainer .hogt {
    background: rgb(255, 190, 190);
}
.kontainer .lagt {
    background: rgb(218, 255, 162);
}
.kontainer .dum {
    background: rgb(216, 0, 0);
}
.kontainer .yipee {
    background: #283f82;
    color: #FFF;
    margin-top: 10px;
}
.kontainer .output {
    background: #FFF;
    padding: 5px;
    border-radius: 3px;
    margin-top: 0;
    margin-bottom: 0;
}
.kontainer img {
    width: 100%;
    background: #FFF;
    padding: 20px;
}
.kontainer .fa {
    display: inline;
    cursor: pointer;
}
.kontainer .animated {
    display: none;
}
.kontainer .visa {
    display: block;
}

.kontainer #bildruta {
    width: 100%;
    height: 460px;
    background: #f3f3f3;
    text-align: center;
    padding: 5px;
}
.kontainer #bildruta img {
    width: 300px;
    padding: 0;
    border: 6px solid #d8c96e;
    border-radius: 10px;
}
.kontainer img.miniatyr {
    padding: 0;
    border: 1px solid salmon;
    cursor: pointer;
}
```
{% endtab %}
{% endtabs %}

### Lyssna på andra events

* Istället för att klicka igång en funktion med kod, kan köra den direkt när webbsidan laddas med **onload**
* Andra events lyssnare är:
  * **onmouseout
    **
  * **onmouseover**
  * **onkeydown
    **
  * **onkeypress
    **
  * **onkeyup**
  * osv...

## Mixtra med strängar

###  Metoden .replace()

* Med [replace()](https://devdocs.io/javascript/global_objects/string/replace) kan man byta ut text i en sträng:

```php
<div class="kontainer">
    <h1>Lyssna på knapphändelser</h1>
    <button onclick="säkerEpost()">Ange ditt namn</button>
</div>
<script>
    function säkerEpost() {
        // Läs in namn
        var namn = prompt("Vad är ditt namn?");

        // Byter ut tecken
        var nyttNamn = namn.replace('a', 'o');

        // Skriv ut svaret
        alert("Ditt nya namn är " + nyttNamn);
    }
</script>
```

## Uppgifter

### Uppgift 1

Skapa ett webbsida där användaren kan klicka på en knapp som heter **"Ange adress"**.\
När användaren klickat på knappen kommer en ruta upp som frågar **"Var bor du?"**. \
Skriptet ska sedan skriva ut svaret i loggen.

### Uppgift 2

Skapa ett webbsida där användaren kan klicka på två knappar som heter **"Ange namn" ** och "**Ange adress**".\
Användarens får sedan svara på frågorna. \
Skriptet ska sedan skriva ut svaret "**Hej xxx, du bor på yyy"** med en alert-ruta.

### Uppgift 3

Skapa en webbsida med en bild.\
När användaren har musen på bilden visas en ruta som berättar vad bilden handlar om. Använd **onmouseover**.

### Uppgift 4

Skapa en webbsida där en ruta kommer fram när användaren trycker på en knapp. Rutan säger "Du tryckte på tangentbordet".

### Uppgift 5

Skapa en webbsida där användaren matar sin **epostadress**.\
Byt sedan ut "**@**" mot "**(at)**". Använd [replace()](https://devdocs.io/javascript/global_objects/string/replace).\
Skriv sedan ut resultatet.
