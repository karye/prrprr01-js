# Variabler och datatyper

![](../.gitbook/assets/image.png)

## Video

{% embed url="https://youtu.be/J_XO0AzXE6Q" %}

## Exempel

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Introduktion i Javascript</title>
</head>
<body>

    <script>
        // Skapar en variabel
        var ålder;

        // Fyller variabeln med svaret
        ålder = prompt("Hur gammal är du?");

        // Skriv ut svaret i loggen
        console.log(ålder);
        alert(ålder);

        // Lite roligare utskrift
        console.log("Din ålder är " + ålder);
        alert("Din ålder är " + ålder);

        // Fråga vad heter du?
        var namn;
        namn = prompt("Vad heter du?");
        //alert("Hej, du heter " + namn);

        // Skriv ut: Hej ..., din ålder är ...
        alert("Hej " + namn + ", din ålder är " + ålder + " år.");

        // Alternativ 2, på en ny rad: \n
        alert("Hej " + namn + ".\nDin ålder är " + ålder + " år.");
    </script>
</body>
</html>
```

## Datatyper

### Strängar och tal

* prompt() levererar data som användaren matat in, det är av datatypen **string**
* Om man vill ha ett tal måste den inmatade texten omvandlas till **int**

```php
<script>
    // Läs året just nu
    var år;
    år = prompt("Vilket år det nu?");

    // Läs in ålder
    var ålder;
    ålder = prompt("Hur gammal är du?");

    // Svara vilket år blir man 20
    var svaret;
    svaret = 20 - parseInt(ålder) + parseInt(år);
    confirm("Du blir tjugo år " + svaret);
</script>
```

### Lista på vanligaste datatyper

| Datatyp | Beskrivning | Förklaring                                                     |
| ------- | ----------- | -------------------------------------------------------------- |
| Boolean | 1 eller 0   | sant eller falskt                                              |
| Number  | tal         | positiva eller negativa tal                                    |
| String  | sträng      | kan innehålla både bokstäver och siffror                       |
| Array   | matris      | innehåller flera variabler som var och en kan ha olika datatyp |

## Uppgifter

### Uppgift 1

Skapa ett skript som frågar användaren **två ord,** efter varandra. \
Skriptet ska sedan presentera en mening med orden i **omvänd** ordning.

### Uppgift 2

Skriv ett skript som frågar hur långt Max och Jakob hoppade. \
Skriv därefter ut hur mycket längre som Max hoppade.

### Uppgift 3

Skapa ett skript som ber användaren mata in sin **lön **i veckan. \
Skriptet räknar sen ut månadslön, dvs **veckoLön \* 12**. \
Skriptet ska sedan berätta för användaren: "**Din månadslön blir ...**".

### Uppgift **4**

Skapa ett skript som ber användaren vilket **årtal **det är i år. \
Skriptet ska sedan berätta årtalet om 10 år: "**Om tio år är det ...**".

### Uppgift 5

Skapa ett skript som frågar användaren hur **långt **hen kan hoppa mätt i meter. \
Skriptet ska sen berätta hur mycket längre världsrekordet är (8,90 meter): "**1968 hoppade Bob Beamon  ... m längre än dig!**". Använd funktionen [parseFloat()](https://devdocs.io/javascript/global_objects/parsefloat).

### Uppgift 6

Skapa ett skript som frågar användaren vilket **årtal **det är.\
Skriptet ska sedan berätta hur många år det är kvar till år **2100**.

### Uppgift 7

Skriv ett skript som tar en siffra som innehåller dagens temperatur i Celsius. \
Skriptet ska sedan presentera hur många grader Fahrenheit det motsvarar enligt följande mall: "**100 grader Celsius motsvarar 212 grader Fahrenheit**". \
Formeln för omvandlingen är **F = (9/5)\*C + 32** där F står för grader Fahrenheit och C för grader Celsius.

### Uppgift 8

Skapa ett nytt skript så att man kan omvandla från F° till C°. Formeln är **C = (F - 32)\*5/9.**

### Uppgift 9

Skapa ett skript för beräkna **kostnaden **för att hyra bil hos en biluthyrningsfirma. \
Startavgiften för att hyra bilen är **500:-**, därefter kostar det ytterligare **5:-/km** och **400:-** för varje extra dag förutom den första. \
Skriptet ska fråga hur **många dagar **man vill hyra bilen och hur **många kilometer** man vill köra.\
Skriptet ska sedan presentera den totala **hyran**.

### Uppgift 10

Skapa ett skript som ber användaren mata in lönen för **3** anställda. \
Skriptet ska sedan presentera **medellönen** för personalen.
