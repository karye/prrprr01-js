---
description: Träna på läsa och skriva direkt på webbsida
---

# Labbar

## Syfte

* Träna på att läsa av innehållet (värdet) i ett input-ruta
* Träna på att skilja mellan ett element på webbsidan och en variabel i koden
* Träna på att skriva i en textarea 
* Träna på att skilja mellan lokala och globala variabler

## Läsa input och skriva i textarea

### Labb 1

![](<../../.gitbook/assets/image (22).png>)

* Spara ned CSS-filen och uppgift1.html
* Läs kommentarerna och fyll i koden som saknas

{% tabs %}
{% tab title="labb1.html" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Variabler och funktioner</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Variabler och funktioner</h1>
        <label>Användarnamn <input type="text" class="anamn"></label>
        <label>Lösenord <input type="password" class="lösen"></label>
        <button class="btn btn-primary">Logga in</button>
        <label>Svar <textarea></textarea></label>
    </div>
    <script>
        // Koden som körs först
        // Element som vi behöver komma åt
        const rutaAnamn = document.querySelector(".anamn");
        const rutaLösen = document.querySelector(".lösen");
        const knappKontroll = document.querySelector(".btn-warning");
        const knappDemo = document.querySelector(".btn-info");
        const knappLoggaIn = document.querySelector(".btn-primary");
        const knappTöm = document.querySelector(".btn-danger");
        const rutaSvar = document.querySelector("textarea");

        // Kod som körs när användaren klickar på knappen Logga in
        knappLoggaIn.addEventListener("click", function () {
            // Läs av användarnamnet och lagra i en lokal variabel

            // Läs av lösenordet och lagra i en lokal variabel

            // Skriv ut användarnamnet i loggen

            // Skriv ut lösenordet i loggen

            // Skriv ut användarnamnet i rutaSvar

            // Skriv ut lösenordet i rutaSvar

            // Skriv ut i rutaSvar "Ditt användarnamn är ... och ditt lösenord är ..."
        })
    </script>
</body>
</html>
```
{% endtab %}

{% tab title="labbar.css" %}
```css
@import url('https://fonts.googleapis.com/css?family=Source+Sans+Pro|Damion|Roboto+Slab&display=swap');

/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}

body {
    background: url("https://images.unsplash.com/photo-1555949963-aa79dcee981c?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80") fixed;
}
.kontainer {
    width: 700px;
    padding: 2em;
    margin: 1em auto;
    min-height: 89vh;
    background: #f5f5f5e0;
    border-radius: 5px;
    font-family: 'Roboto Slab', serif;
    border: 1px solid #a0a0a0;
    box-shadow: 1px 1px 4px #686868;
}
nav {
    padding: 1em 0;
}
main {
    padding: 1em;
    font-size: 0.9em;
    color: #7b7b7b;
}

nav .anamn {
    margin-left: auto;
    padding: 10px;
    font-weight: bold;
}

label {
    color: #555555;
    font-weight: bold;
    display: grid;
    grid-template-columns: 1fr 2fr;
    margin: 10px 0;
    padding: 0;
}
input, textarea {
    padding: 0.5em;
    margin-top: -0.4em;
    font-style: italic;
    border-radius: 0.3em;
    border: 2px solid #55a5d2;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
textarea {
    height: 10em;
}
button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
}
img {
    width: 30px;
}
.grid {
    display: grid;
    grid-template-columns: 1fr 1fr 2fr;
    gap: 1em;
}
.result {
    text-align: right;
    color: #cc2a2a;
    font-style: italic;
    align-self: center;
}


h1, h2, h3, h4, h5, h6 {
    font-size: 1em;
    font-weight: bold;
}
h1, h2, h3, p {
    margin: 0.8em 0;
}
h1 {
    color: #555555;
    font-size: 2em;
}

table {
    width: 100%;
    border-collapse: collapse;
    border: 1px solid #b89e44;
    background: #ffffffa3;
    padding: 2em;
    border-radius: 5px;
}
th, td {
    padding: 0.5em;
    text-align: left;
}
th {
    background: #FFF;
    padding: 1em 0.5em;
}
tr:nth-child(even) {
    background: #f1f1f1;
}
/* tr:nth-child(odd) {
    background: #FFF;
} */
table .fa {
    color: #55a5d2;
}
table img {
    width: 50px;
}

.grid-6 {
    padding: 2em;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
    gap: 2em;
}
```
{% endtab %}
{% endtabs %}

## Läsa, skriva och villkor

### Labb 2

![](<../../.gitbook/assets/image (23).png>)

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Variabler och funktioner</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Variabler och funktioner</h1>
        <label>Användarnamn <input type="text" class="anamn"></label>
        <label>Lösenord <input type="password" class="lösen"></label>
        <button class="btn btn-warning">Kontrollera lösenordet</button>
        <button class="btn btn-info">Demo</button>
        <button class="btn btn-primary">Logga in</button>
        <button class="btn btn-danger">Töm rutorna</button>
        <label>Svar <textarea></textarea></label>
    </div>
    <script>
        // Koden som körs först
        // Element som vi behöver komma åt
        const rutaAnamn = document.querySelector(".anamn");
        const rutaLösen = document.querySelector(".lösen");
        const knappKontroll = document.querySelector(".btn-warning");
        const knappDemo = document.querySelector(".btn-info");
        const knappTöm = document.querySelector(".btn-danger");
        const rutaSvar = document.querySelector("textarea");

        // Kod som körs när användaren klickar på knappen Logga in
        knappLoggaIn.addEventListener("click", function () {
            // Läs av användarnamnet och lagra i en lokal variabel

            // Läs av lösenordet och lagra i en lokal variabel

            // Skriv ut användarnamnet i loggen

            // Skriv ut lösenordet i loggen

            // Skriv ut användarnamnet i rutaSvar

            // Skriv ut lösenordet i rutaSvar

            // Är användarnamnet "admin" skriv ut "Användarnamn ok" i rutaSvar

            // Är användarnamnet "admin" och lösenordet "super" skriv ut "Inloggad!" i rutaSvar
            // Annars skriv ut "Något är fel!"

        })
    </script>
</body>
</html>
```

### Labb 3

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Variabler och funktioner</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Variabler och funktioner</h1>
        <label>Användarnamn <input type="text" class="anamn"></label>
        <label>Lösenord <input type="password" class="lösen"></label>
        <button class="btn btn-primary">Logga in</button>
        <label>Svar <textarea></textarea></label>
    </div>
    <script>
        // Koden som körs först
        // Element som vi behöver komma åt
        const rutaAnamn = document.querySelector(".anamn");
        const rutaLösen = document.querySelector(".lösen");
        const knappKontroll = document.querySelector(".btn-warning");
        const knappDemo = document.querySelector(".btn-info");
        const knappLoggaIn = document.querySelector(".btn-primary");
        const knappTöm = document.querySelector(".btn-danger");
        const rutaSvar = document.querySelector("textarea");

        // Kod som körs när användaren klickar på knappen Logga in
        knappLoggaIn.addEventListener("click", function () {
            // Läs av användarnamnet och lagra i en lokal variabel

            // Läs av lösenordet och lagra i en lokal variabel

            // Skriv ut användarnamnet i loggen

            // Skriv ut lösenordet i loggen

            // Är användarnamn (valfritt) och lösenord (valfritt) ifyllda
            // Skriv "Välkommen ... du är inloggad" i rutaSvar

        })
    </script>
</body>
</html>
```

### Labb 4

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Variabler och funktioner</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Variabler och funktioner</h1>
        <label>Användarnamn <input type="text" class="anamn"></label>
        <label>Lösenord <input type="password" class="lösen"></label>
        <button class="btn btn-primary">Logga in</button>
        <button class="btn btn-danger">Töm rutorna</button>
        <label>Svar <textarea></textarea></label>
    </div>
    <script>
        // Koden som körs först
        // Element som vi behöver komma åt
        const rutaAnamn = document.querySelector(".anamn");
        const rutaLösen = document.querySelector(".lösen");
        const knappKontroll = document.querySelector(".btn-warning");
        const knappDemo = document.querySelector(".btn-info");
        const knappLoggaIn = document.querySelector(".btn-primary");
        const knappTöm = document.querySelector(".btn-danger");
        const rutaSvar = document.querySelector("textarea");

        // Kod som körs när användaren klickar på knappen Logga in

            // Läs av användarnamnet och lagra i en lokal variabel

            // Läs av lösenordet och lagra i en lokal variabel

            // Skriv ut användarnamnet i loggen

            // Skriv ut lösenordet i loggen

            // Skriv ut "Välkommen ... du är inloggad" i rutaSvar



        // Kod som körs när användaren klickar på knappen Töm

            // Töm alla rutor genom att skriva tomt "" i rutorna
        
    </script>
</body>
</html>
```

### Labb 5

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Variabler och funktioner</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Variabler och funktioner</h1>
        <label>Användarnamn <input type="text" class="anamn"></label>
        <label>Lösenord <input type="password" class="lösen"></label>
        <button class="btn btn-warning">Kontrollera lösenordet</button>
        <label>Svar <textarea></textarea></label>
    </div>
    <script>
        // Koden som körs först
        // Element som vi behöver komma åt
        const rutaAnamn = document.querySelector(".anamn");
        const rutaLösen = document.querySelector(".lösen");
        const knappKontroll = document.querySelector(".btn-warning");
        const knappDemo = document.querySelector(".btn-info");
        const knappLoggaIn = document.querySelector(".btn-primary");
        const knappTöm = document.querySelector(".btn-danger");
        const rutaSvar = document.querySelector("textarea");

        // Kod som körs när användaren klickar på knappen Kontrollera lösenordet

            // Läs av användarnamnet och lagra i en lokal variabel

            // Läs av lösenordet och lagra i en lokal variabel

            // Skriv ut användarnamnet i loggen

            // Skriv ut lösenordet i loggen

            // Är användarnamnet tomt
            // Skriv ut en varning i rutaSvar

            // Är lösenordet "123" eller "1234" eller "12345"
            // skriv ut en varning i rutaSvar,
            // annars skriv ut "Bra lösenord!"


    </script>
</body>
</html>
```

### Labb 6

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Variabler och funktioner</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Variabler och funktioner</h1>
        <label>Användarnamn <input type="text" class="anamn"></label>
        <label>Lösenord <input type="password" class="lösen"></label>
        <button class="btn btn-warning">Kontrollera lösenordet</button>
        <label>Svar <textarea></textarea></label>
    </div>
    <script>
        // Koden som körs först
        // Element som vi behöver komma åt
        const rutaAnamn = document.querySelector(".anamn");
        const rutaLösen = document.querySelector(".lösen");
        const knappKontroll = document.querySelector(".btn-warning");
        const knappDemo = document.querySelector(".btn-info");
        const knappLoggaIn = document.querySelector(".btn-primary");
        const knappTöm = document.querySelector(".btn-danger");
        const rutaSvar = document.querySelector("textarea");

        // Kod som körs när användaren klickar på knappen Kontrollera lösenordet

            // Läs av användarnamnet och lagra i en lokal variabel

            // Läs av lösenordet och lägg i en lokal variabel

            // Räkna ut längden på användarnamnet och lagra i ny lokal variabel 
            // längdAnamn
            // Se https://devdocs.io/javascript/global_objects/string/length

            // Skriv ut "Ditt användarnamn är ... och är ... långt" i rutaSvar

            // Räkna ut längden på lösenordet och lägg i ny variabeln längdLösen

            // Skriv ut "Ditt lösenord är ... och är ... långt" i rutaSvar (Skriv inte över raden om användarnamnet)

            // Om Användarnamnet är längre än 5 tecken skriv ut en rad till:
            // "Användarnamnet är minst 6 tecken långt"

            // Om lösenordet är längre än 7 tecken skriv ut en rad till:
            // "Användarnamnet är minst 8 tecken långt"

    </script>
</body>
</html>
```

## Globala variabler

### Labb 7

![](<../../.gitbook/assets/image (25).png>)

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Variabler och funktioner</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Variabler och funktioner</h1>
        <label>Lösenord <input type="password" class="lösen1"></label>
        <button class="btn btn-primary">Spara första lösenordet</button>
        <label>Upprepa lösenord <input type="password" class="lösen2"></label>
        <button class="btn btn-warning">Kontrollera lösenorden</button>
        <label>Svar <textarea></textarea></label>
    </div>
    <script>
        // Koden som körs först
        // Element som vi behöver komma åt
        const rutaLösen1 = document.querySelector(".lösen1");
        const rutaLösen2 = document.querySelector(".lösen2");
        const knappKontroll = document.querySelector(".btn-warning");
        const knappSpara = document.querySelector(".btn-primary");
        const rutaSvar = document.querySelector("textarea");

        // Skapa global variabel lösen1


        // Kod som körs när användaren klickar på knappen Spara lösenordet

            // Läs av lösenordet och lagra i den globala variabeln

            // Skriv ut lösenordet i loggen

            // Om lösenordet är kortare än 6 tecken skriv ut svaret
            // Lösenordet för kort 
            // Annars lösenordet ok

        // Kod som körs när användaren klickar på knappen Kontrollera lösenordet

            // Läs av lösenordet och lagra i en lokal variabel

            // Skriv ut lösenordet i loggen

            // Om lösenordet är lika med första lösenordet skriv ut en rad till:
            // "Lösenorden stämmer"

    </script>
</body>
</html>
```

## Använda en metod

### Labb 8

![](<../../.gitbook/assets/image (26).png>)

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Globala och lokala variabler</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Globala och lokala variabler</h1>
        <label>Längd på lösenordet <input type="text" class="längd" required></label>
        <button class="btn btn-primary">Ange längd</button>
        <label>Lösenord <input type="password" class="lösen" required></label>
        <button class="btn btn-warning">Kontrollera lösenordet</button>
        <label>Svar <textarea></textarea></label>
    </div>
    <script>
        // Koden som körs först
        // Element som vi behöver komma åt
        const rutaAnamn = document.querySelector(".anamn");
        const rutaLösen = document.querySelector(".lösen");
        const knappKontroll = document.querySelector(".btn-warning");
        const knappLoggaIn = document.querySelector(".btn-primary");
        const rutaSvar = document.querySelector("textarea");

        // Skapa globala variabel längd
        

        // Kod som körs när användaren klickar på knappen 

            // Läs av längden och lagra i globala variabeln längd


        // Kod som körs när användaren klickar på knappen Kontrollera lösenordet

            // Läs av lösenordet och lagra i en lokal variabel

            // Räkna ut längden på lösenordet och lägg i ny variabel längdLösen
            // Använd https://devdocs.io/javascript/functions/arguments/length

            // Skriv ut "Ditt lösenord är ... och är ... långt" i rutaSvar (Skriv inte över)

            // Om lösenordet är längre globala variabeln "längd" skriv ut en rad till:
            // "Lösenordet är minst ... tecken långt"

    </script>
</body>
</html>
```

## Extra labbar

### Labb 9

Bygg ihop det som användaren skriver i rutorna till en mening och skriv ut i svarsrutorna.\
När man trycker på första knappen skrivs texten i ruta 1, tryck på andra knappen skrivs texten ut i rutan 2.

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Läsa in och skriva ut</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Läsa in och skriva ut</h1>
        <label>Namn <input type="text" class="anamn"></label>
        <label>Ålder <input type="text" class="ålder"></label>
        <label>Intresse <input type="text" class="intresse"></label>

        <button class="knapp1 btn btn-primary">Skicka till ruta 1</button>
        <button class="knapp2 btn btn-warning">Skicka till ruta 2</button>

        <label>Svar 1 <textarea class="svar1"></textarea></label>
        <label>Svar 2 <textarea class="svar2"></textarea></label>
    </div>
    <script>
        /* Börja koda här */
        // Hitta elementen på webbsidan


    </script>
</body>
</html>
```

### Labb 10

Hårdkoda förts värdet på variabeln **intresse**.\
När användaren trycker på knappen skrivs en mening ut innehållande text från input-rutorna ihop med värdet på variabeln **intresse**.

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Läsa in och skriva ut</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Läsa in och skriva ut</h1>
        <label>Namn <input type="text" class="anamn"></label>
        <label>Ålder <input type="text" class="ålder"></label>

        <button class="knapp1 btn btn-primary">Skicka</button>
        <button class="knapp2 btn btn-warning">Töm</button>

        <label>Svar 1 <textarea class="svar1"></textarea></label>
        <label>Svar 2 <textarea class="svar2"></textarea></label>
    </div>
    <script>
        // En fast hårdkodad variabel
        var intresse = "fotboll";

        /* Börja koda här */
        // Hitta elementen på webbsidan
    </script>
</body>
</html>
```

### Labb 11

Här skall användaren gissa det hårdkodade ordet.\
Om användaren gissade rätt skrivs "Rätt svar" i första rutan, om fel skrivs "Fel svar" i andra rutan.

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Läsa in och skriva ut</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
    <link rel="stylesheet" href="labbar.css">
</head>
<body>
    <div class="kontainer">
        <h1>Läsa in och skriva ut</h1>
        <label>Namn <input type="text" class="anamn"></label>
        <label>Gissa ett ord <input type="text" class="gissning"></label>

        <button class="knapp1 btn btn-primary">Gissa</button>
        <button class="knapp2 btn btn-warning">Töm</button>

        <label>Rätt svar <textarea class="svar1"></textarea></label>
        <label>Fel svar <textarea class="svar2"></textarea></label>
    </div>
    <script>
        // En fast hårdkodad variabel
        var intresse = "fotboll";

        /* Börja koda här */
        // Hitta elementen på webbsidan


    </script>
</body>
</html>
```
