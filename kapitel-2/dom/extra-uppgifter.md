---
description: För att träna på querySelector(), .value, sätta samman text och if-satser
---

# Extra uppgifter

## Grundkoden

![](<../../.gitbook/assets/image (42).png>)

{% tabs %}
{% tab title="JavaScript" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>querySelector() och value</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="kontainer">
        <h1>Läsa av input-rutor</h1>
        <h2>Input</h2>
        <label>Ange förnamn <input type="text"></label>
        <button>Skicka</button>
        <h2>Output</h2>
        <label>Svar <textarea></textarea></label>
    </div>
    <script>
        const rutaNamn = document.querySelector("input");
        const knappen = document.querySelector("button");
        const rutaSvar = document.querySelector("textarea");

        knappen.addEventListener("click", function () {
            rutaSvar.value = "Hej på dig " + rutaNamn.value;
        })
    </script>
</body>
</html>
```
{% endtab %}

{% tab title="style.css" %}
```css
@import url('https://fonts.googleapis.com/css?family=Open+Sans');

/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
body {
    background: #F9F6EB url("https://images.unsplash.com/photo-1580584126903-c17d41830450?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1837&q=80");;
}
.kontainer {
    width: 600px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Open Sans', sans-serif;
    border: 1px solid #ddd;
    box-shadow: 0 0 12px #f0e9d1;
}
.kontainer #myCanvas {
    border: 1px solid;
}
.kol2 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr;
    grid-gap: 1em;
}
.kol3 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr 1fr;
    grid-gap: 1em;
}
.kol8 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: auto auto auto auto auto auto auto auto;
    grid-gap: 1em;
}
.kontainer label {
    display: grid;
    grid-template-columns: 1fr 2fr;
    margin: 10px 0;
    padding: 0;
}
.kontainer input, .kontainer textarea {
    padding: 0.7em;
    border-radius: 0.3em;
    border: 1px solid #ccc;
    font-weight: bold;
    box-shadow: inset 0 2px 2px #0000001a;
}
.kontainer textarea {
    height: 10em;
    width: 100%;
}
.kontainer button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
    cursor: pointer;
}
.kontainer h1, .kontainer h2, .kontainer h3 {
    margin: 0.5em 0;
}
.kontainer p {
    background: #FFF;
    padding: 5px;
    border-radius: 3px;
    margin-top: 0;
    margin-bottom: 0;
    font-size: 14px;
}
.kontainer .hogt {
    background: #ffbebe;
}
.kontainer .lagt {
    background: #daffa2;
}
.kontainer .dum {
    background: #d80000;
}
.kontainer .yipee {
    background: #283f82;
    color: #FFF;
    margin-top: 10px;
}
.kontainer .output {
    background: #FFF;
    padding: 5px;
    border-radius: 3px;
    margin-top: 0;
    margin-bottom: 0;
}
.kontainer img {
    width: 100%;
    background: #FFF;
    padding: 20px;
}
.kontainer .fa {
    display: inline;
    cursor: pointer;
}
.kontainer .animated {
    display: none;
}
.kontainer .visa {
    display: block;
}
.kontainer #bildruta {
    width: 100%;
    height: 460px;
    background: #f3f3f3;
    text-align: center;
    padding: 5px;
}
.kontainer #bildruta img {
    width: 300px;
    padding: 0;
    border: 6px solid #d8c96e;
    border-radius: 10px;
}
.kontainer img.miniatyr {
    padding: 0;
    border: 1px solid salmon;
    cursor: pointer;
}

/* blinking cursor */
#cursor {
    background: lime;
    line-height: 17px;
    margin-left: 3px;
    animation: blink 0.8s infinite;
    width: 7px;
    height: 15px;
}

@keyframes blink {
    0% {
        background: #222
    }
    50% {
        background: lime
    }
    100% {
        background: #222
    }
}
```
{% endtab %}
{% endtabs %}

## Uppgifter

### Uppgift 1

![](<../../.gitbook/assets/image (39).png>)

### Uppgift 2

![](<../../.gitbook/assets/image (40).png>)

### Uppgift 3

![](<../../.gitbook/assets/image (41).png>)

### Uppgift 4

![](<../../.gitbook/assets/image (43).png>)

### Uppgift 5

![](<../../.gitbook/assets/image (44).png>)

