---
description: Hur man ändrar på en webbsida
---

# Läsa/skriva i webbsida

## Mixtra med sidan

### document.title

* Med [document.title](https://devdocs.io/dom/document/title) kan man ändra titel på fliken i webbläsaren:

```php
<div class="kontainer">
    <h1>Lyssna på knapphändelser</h1>
    <button onclick="nyTitel()">Ny titel</button>
</div>
<script>
    function nyTitel() {
        document.title = "Moaahahaha!!!";
    }
</script>
```

### document.body.style.cssText

* Med [document.body](https://devdocs.io/dom/document/body) kommer man åt body
* Då kan man styla body med [style.cssText](https://devdocs.io/dom/htmlelement/style):

```php
<div class="kontainer">
    <h1>Lyssna på knapphändelser</h1>
    <button onclick="nyBakgrund()">Byt bakgrundsfärg</button>
</div>
<script>
    function nyBakgrund() {
        document.body.style.cssText = "background: purple;";
    }
</script>
```

### document.body.innerHTML

* Man kan infoga vilken HTML som helst med [innerHTML](https://devdocs.io/dom/element/innerhtml):

```php
<div class="kontainer">
    <h1>Lyssna på knapphändelser</h1>
    <button onclick="nySida()">Byt sida</button>
</div>
<script>
    function nySida() {
        document.body.innerHTML = "<h1>Nu är sidan tom!</h1>";
    }
</script>
```

### Övning

```php
<body>
    <div class="kontainer">
        <h1>Ändra bakgrundsfärgen</h1>
        <button onclick="nyBakgrund()">Ändra bakgrund</button>
    </div>
    <script>
        function nyBakgrund() {
            var färg = prompt("Ange en färg");
            console.log(färg);
            document.body.style.cssText = "background:" + färg;
        }
    </script>
</body>
```

## Uppgifter

### Uppgift 1

Skapa en webbsida där användaren matar in en **text**.\
Byt sedan ut sidans **titel **med texten.

### Uppgift 2

Skapa en webbsida med två knappar:\
\- **Knapp 1** gör att sidan blir **mörk**.\
\- **Knapp 2** gör att sidan blir **ljus**.

### Uppgift 3

Skapa en webbsida med en knapp som heter "**Danger Zone**".\
Klicka användaren på knappen så visas en animerad bild i **bakgrunden**.\
Se [https://giphy.com](https://giphy.com).

### Uppgift 4

Skapa en webbsidan med **3 knappar**.\
Varje knapp byter ut **bakgrundsbild **på sidan.\
Ladda ned 3 bilder från [unsplash.com](https://unsplash.com).

### Uppgift 5

Skapa en webbsida med en knapp.\
När användaren klickar på knappen visas ett valfritt **Youtube**-klipp.\
(Ej som bakgrundsbild).

### Uppgift 6

Skapa en webbsida med en knapp med texten "**Hemlig dikt**".\
När användaren klickar på knappen visas dikten "I rörelse":

```
Den mätta dagen, den är aldrig störst.
Den bästa dagen är en dag av törst.

Nog finns det mål och mening i vår färd -
men det är vägen, som är mödan värd.

Det bästa målet är en nattlång rast,
där elden tänds och brödet bryts i hast.

På ställen, där man sover blott en gång,
blir sömnen trygg och drömmen full av sång.

Bryt upp, bryt upp! Den nya dagen gryr.
Oändligt är vårt stora äventyr.
```

### Uppgift 7

Skapa en webbsida med en knapp (**Knapp 1**):\
Knapp 1 gör att sidan får en ny text med en till knapp (**Knapp 2**).\
Knapp 2 gör att sidan får vit text mot svart bakgrund.
