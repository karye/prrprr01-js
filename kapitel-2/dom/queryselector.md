---
description: Hitta element på sidan
---

# Hitta med querySelector()

## Välja ut element med querySelector()

### **Genomgång**

{% embed url="https://youtu.be/3oOKAJTD2F8" %}

### Läsa av en input

* En webbsida med en input-ruta och en knapp
* Först väljer vi ut elementet med **querySelector()**
* Sedan läser vi av innehållet med **.value**

{% tabs %}
{% tab title="Startkod" %}
```php
<body>
    <div class="kontainer">
        <h1>Läsa av input-rutor</h1>
        <h2>Input</h2>
        <label>Ange förnamn <input type="text"></label>
        <button onclick="läsav()">Skicka</button>
    </div>
    <script>
        function läsav() {
            // Väljer ut element
            const elementInput = document.querySelector("input");

            // Läser av värdet i input-rutan
            var fnamn = elementInput.value;

            // Skriver ut värdet
            console.log("fnamn", fnamn);
        }
    </script>
</body>
```
{% endtab %}

{% tab title="style.css" %}
```css
@import url('https://fonts.googleapis.com/css?family=Open+Sans');

/* Enkel CSS-reset */
html {
    box-sizing: border-box;
}
*, *:before, *:after {
    box-sizing: inherit;
}
body, h1, h2, h3, h4, h5, h6, p, ul {
    margin: 0;
    padding: 0;
}
body {
    background: #F9F6EB;
}
.kontainer {
    width: 600px;
    padding: 2em;
    margin: 3em auto;
    background: #fff;
    border-radius: 5px;
    font-family: 'Open Sans', sans-serif;
    border: 1px solid #ddd;
    box-shadow: 0 0 12px #f0e9d1;
}
.kontainer #myCanvas {
    border: 1px solid;
}
.kol2 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr;
    grid-gap: 1em;
}
.kol3 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: 1fr 2fr 1fr;
    grid-gap: 1em;
}
.kol8 {
    margin: 1em 0;
    display: grid;
    grid-template-columns: auto auto auto auto auto auto auto auto;
    grid-gap: 1em;
}
.kontainer label {
    display: grid;
    grid-template-columns: 1fr 2fr;
    margin: 10px 0;
    padding: 0;
}
.kontainer input, .kontainer textarea {
    padding: 0.7em;
    border-radius: 0.3em;
    border: 1px solid #ccc;
    font-weight: bold;
    box-shadow: inset 0 2px 2px rgba(0, 0, 0, 0.1);
}
.kontainer textarea {
    height: 10em;
    width: 100%;
}
.kontainer button {
    margin: 1em 0;
    padding: 0.7em;
    border-radius: 0.3em;
    border: none;
    font-weight: bold;
    color: #FFF;
    background-color: #55a5d2;
    cursor: pointer;
}
.kontainer h1, .kontainer h2, .kontainer h3 {
    margin: 0.5em 0;
}
.kontainer p {
    background: #FFF;
    padding: 5px;
    border-radius: 3px;
    margin-top: 0;
    margin-bottom: 0;
    font-size: 14px;
}
.kontainer .hogt {
    background: rgb(255, 190, 190);
}
.kontainer .lagt {
    background: rgb(218, 255, 162);
}
.kontainer .dum {
    background: rgb(216, 0, 0);
}
.kontainer .yipee {
    background: #283f82;
    color: #FFF;
    margin-top: 10px;
}
.kontainer .output {
    background: #FFF;
    padding: 5px;
    border-radius: 3px;
    margin-top: 0;
    margin-bottom: 0;
}
.kontainer img {
    width: 100%;
    background: #FFF;
    padding: 20px;
}
.kontainer .fa {
    display: inline;
    cursor: pointer;
}
.kontainer .animated {
    display: none;
}
.kontainer .visa {
    display: block;
}

.kontainer #bildruta {
    width: 100%;
    height: 460px;
    background: #f3f3f3;
    text-align: center;
    padding: 5px;
}
.kontainer #bildruta img {
    width: 300px;
    padding: 0;
    border: 6px solid #d8c96e;
    border-radius: 10px;
}
.kontainer img.miniatyr {
    padding: 0;
    border: 1px solid salmon;
    cursor: pointer;

```
{% endtab %}
{% endtabs %}

### Läsa av två input-rutor

```php
<body>
    <div class="kontainer">
        <h1>Läsa av input-rutor</h1>
        <h2>Input</h2>
        <label>Ange förnamn <input class="fnamn" type="text"></label>
        <label>Ange efternamn <input class="enamn" type="text"></label>
        <button onclick="läsav()">Skicka</button>
    </div>
    <script>
        function läsav() {
            // Väljer ut element
            const rutaFnamn = document.querySelector(".fnamn");
            const rutaEnamn = document.querySelector(".enamn");

            // Läser av värdet i input-rutan
            var fnamn = rutaFnamn.value;
            var enamn = rutaEnamn.value;

            // Skriver ut värdet
            console.log("fnamn", fnamn);
            console.log("enamn", enamn);

            // Skriv ut bekräftelse
            alert("Hej " + fnamn + " " + enamn + ", trevlig att se dig!");
        }
    </script>
</body>
```

### Skriva i en input-ruta

```php
<body>
    <div class="kontainer">
        <h1>Läsa av input-rutor</h1>
        <h2>Input</h2>
        <label>Ange lösenord <input class="lösen" type="text"></label>
        <button onclick="läsav()">Visa hemlig text</button>

        <h2>Output</h2>
        <label>Svar <input class="svar" type="text"></label>
    </div>
    <script>
        function läsav() {
            // Väljer ut element
            const rutaLösen = document.querySelector(".lösen");
            const rutaSvar = document.querySelector(".svar");

            // Läser av värdet i input-rutan
            var lösen = rutaLösen.value;

            // Svar
            if (lösen == "12345") {
                rutaSvar.value = "Du är himla smart";
            } else {
                rutaSvar.value = "Du suger!";
            }
        }
    </script>
</body>
```

## Uppgifter

### Uppgift 1

Skapa ett skript som visar hemligt mobilnr om man kan bevisa att man är människa genom en väl vald fråga.

![](<../../.gitbook/assets/image (17).png>)

### Uppgift 2

Skapa ett skript som frågar efter förnamn, efternamn och ålder.\
Skriptet svarar om användaren får ta körkort eller är omyndig.

![](<../../.gitbook/assets/image (15).png>)
