# Första koden

## Output

### Kommandon

* Med kommandot alert() visas en ruta upp.
* Men kommandot console.log() skriver man ut i konsolen som är dold

```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Introduktion i Javascript</title>
</head>
<body>
    
    <script>
        // Min första rad kod
        alert("Min första rad Javascript!");

        // Andra raden kod
        console.log("Testar lite javascript");
    </script>
</body>
</html>
```
