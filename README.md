---
description: Installera alla verktyg, konfigurera alla inställningar, skapar alla mappar
---

# Introduktion

## Centralt innehåll

### **Undervisningen i kursen ska behandla följande centrala innehåll:**

* **Grundläggande programmering i ett eller flera programspråk varav minst ett av språken är textbaserat.**
* Programmering och dess olika användningsområden ur ett socialt perspektiv inklusive genus, kultur och socioekonomisk bakgrund.
* Programmeringens möjligheter och begränsningar utifrån datorns funktionssätt.
* **Strukturerat arbetssätt för problemlösning och programmering.**
* **Grundläggande kontrollstrukturer, konstruktioner och datatyper.**
* **Arbetsmetoder för förebyggande av programmeringsfel, testning, felsökning och rättning av kod.**
* **Grundläggande datastrukturer och algoritmer.**
* **Gränssnitt för interaktion mellan program och användare.**
* Normer och värden inom programmering, till exempel läsbarhet, dokumentation, testbarhet, rena gränssnitt och nyttan av standard.

## Bra länkar

### Javascript tutorials

* På svenska: [JavaScript på htmlhunden.se](http://htmlhunden.se/dist/04-00-js-intro.html)
* [freecodecamp.org](https://www.freecodecamp.org/learn/javascript-algorithms-and-data-structures/basic-javascript/)
* [javascript.info](https://javascript.info)

### Javascript böcker

* [jsbooks.revolunet.com](https://jsbooks.revolunet.com)
