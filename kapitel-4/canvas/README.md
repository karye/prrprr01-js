---
description: HTML5 canvas är en rityta som man kan göra mycket roligt med
---

# Canvas

## Vad canvas?

En canvas eller rityta skapa med taggen [canvas](https://devdocs.io/html/element/canvas):

![](https://lh6.googleusercontent.com/0NiZ_h6b124mArD42TMJ-HHyrhMr7VZafwcIuSkjzMr31DewOi2kYBzUdg_cnCW0TUHaPO1yq7CthZtfFZuCiz8uJZCdeoBNBVPUDE84Y1VFkm7XmvNsyNBZ3ShMSVJAIs8de512sH0)

### Koden

{% tabs %}
{% tab title="canvas1.html" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rita med canvas</title>
    <link rel="stylesheet" href="./canvas.css">
</head>
<body>
    <canvas></canvas>
    <script>
        // Hitta element på sidan
        const eCanvas = document.querySelector("canvas");

        // Ange bredd och höjd på canvas
        eCanvas.width = 800;
        eCanvas.height = 600;

        // Aktivera ritmotorn
        var ctx = eCanvas.getContext("2d");
    </script>
</body>
</html>
```
{% endtab %}

{% tab title="canvas.css" %}
```css
body {
    background: slategrey;
}
canvas {
    border: 1px solid salmon;
    display: block;
    margin: 10px auto;
    background: #FFF;
}
```
{% endtab %}
{% endtabs %}
