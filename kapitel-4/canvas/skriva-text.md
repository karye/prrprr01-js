# Skriva text

## Ställa in stil och färg

När man ritar väljer man mellan **fill **och **stroke**:

* **fill **står för fyllning
* **stroke **står för kanten
* **lineWidth **anger kanten tjocklek

```php
<canvas></canvas>
<script>
    // Element vi jobbar med
    const eCanvas = document.querySelector("canvas");

    // Ställ in bredd och höjd på canvas
    eCanvas.width = 800;
    eCanvas.height = 600;

    // Aktivera ritmotorn
    var ctx = eCanvas.getContext("2d");

    // Välj en fyllnadsfärg
    ctx.fillStyle = "lightblue";
    ctx.strokeStyle = "purple";
    ctx.lineWidth = 15;

    // Rita en rektangel
    ctx.fillRect(100, 100, 200, 100);
    ctx.strokeRect(400, 100, 200, 150);
</script>
```

![](<../../.gitbook/assets/image (37).png>)

## Att skriva text

På liknande sätt skriver man text**:**

* med **fillText()** skrivs fylld text
* och med **strokeText()** skrivs konturtext

```php
<script>
    // Rita text
    ctx.lineWidth = 4;
    ctx.font = "bold 60px sans-serif";
    ctx.fillText("Canvas är kul!", 100, 400);
    ctx.strokeText("Canvas är kul!", 100, 500);
</script>
```

![](<../../.gitbook/assets/image (38).png>)

## Uppgifter

### Uppgift 1

Välj ett typsnitt på Google Fonts och skrivs ut ditt namn.
