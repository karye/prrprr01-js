# Rita rektanglar och linjer

## Rita en rektangel

![](https://lh4.googleusercontent.com/-9zThx7FmKFgpkyGhN8OBpG9jzBAgkoNidbrPAGPwJw4Zfh0h4d5wGj1SQLTDr7c9VCUx-xJ49-mIEl7thiTe0GTH9wjbKx1jSj_AsODnTuq_USZm3fhcNh9Nqe93OIqhiSyrfiTuJY)

{% tabs %}
{% tab title="Javascript" %}
```php
    <script>
        ...
        
        // Rita en rektangel
        ctx.fillRect(100, 100, 200, 100);
    </script>
```
{% endtab %}
{% endtabs %}

### Rita färgade rektanglar

![](<../../.gitbook/assets/image (28).png>)

{% tabs %}
{% tab title="js" %}
```php
    <script>
        ...
        
        // Rita färgad rektangel
        ctx.fillStyle = "darkblue";
        ctx.fillRect(100, 200, 200, 200);
        ctx.fillStyle = "purple";
        ctx.fillRect(100, 300, 200, 100);
        ctx.fillStyle = "orange";
        ctx.fillRect(100, 400, 200, 100);
    </script>
```
{% endtab %}
{% endtabs %}

### Uppgift 1

* Rita ett färgat rutmönster med rutor 50x50 pixlar:

![](<../../.gitbook/assets/image (27).png>)

### Uppgift 2

* Rita den svenska flaggan:

![](<../../.gitbook/assets/image (31).png>)

## Rita en bana

![](<../../.gitbook/assets/image (29).png>)

{% tabs %}
{% tab title="js" %}
```php
    <script>
        ...

        // Rita linje
        // Börja en bana
        ctx.beginPath();
        
        // Flytta pennan
        ctx.moveTo(400, 300);
        
        // Rita med pennan
        ctx.lineTo(500, 300);
        ctx.lineTo(400, 400);
        ctx.lineTo(500, 400);

        // Färglägg lijen
        ctx.lineWidth = 5;
        ctx.stroke();
    </script>
```
{% endtab %}
{% endtabs %}

### Uppgift 3

* Rita ett hus:

![](<../../.gitbook/assets/image (36).png>)

### Uppgift 4

* Rita den tjeckiska flaggan:

![](<../../.gitbook/assets/image (30).png>)
