# Rita cirklar

## Rita en cirkel

![](<../../.gitbook/assets/image (32).png>)



{% tabs %}
{% tab title="canvas3.html" %}
```php
<!DOCTYPE html>
<html lang="sv">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rita med canvas</title>
    <link rel="stylesheet" href="./canvas.css">
</head>
<body>
    <canvas></canvas>
    <script>
        // Hitta element på sidan
        const eCanvas = document.querySelector("canvas");

        // Ange bredd och höjd på canvas
        eCanvas.width = 800;
        eCanvas.height = 600;

        // Aktivera ritmotorn
        var ctx = eCanvas.getContext("2d");
        
        // Rita en cirkel
        ctx.beginPath();
        ctx.arc(300, 200, 200, 0, 2 * Math.PI);
        ctx.stroke();
        
        // Rita en fylld cirkel
        ctx.beginPath();
        ctx.arc(600, 200, 100, 0, 2 * Math.PI);
        ctx.fill();
    </script>
</body>
</html>
```
{% endtab %}
{% endtabs %}

### Rita en smiley

![](<../../.gitbook/assets/image (35).png>)

```php
<script>
    // Rita en smiley
    ctx.lineWidth = 3;
    ctx.beginPath();
    ctx.arc(600, 450, 100, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(550, 420, 20, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(650, 420, 20, 0, 2 * Math.PI);
    ctx.stroke();
    ctx.beginPath();
    ctx.arc(600, 480, 20, Math.PI/4, Math.PI/2 + Math.PI/4);
    ctx.stroke();
</script>    
```

### Uppgift 2

* Rita en hänga gubbe:

![](<../../.gitbook/assets/image (34).png>)
