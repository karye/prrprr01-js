---
description: När man i förväg vet hur många gånger koden skall upprepas
---

# for-loopen

## **for-loopen**

Nästa varianten vi ska titta på är en [for-loop](https://devdocs.io/javascript/statements/for):

```php
<script>
for (var i = 1; i < 10; i = i + 1) {
   console.log("Detta är rad", i);
}
</script>
```

I detta exempel kommer vi att få 19 rader utskrivna. [for-loopen](https://devdocs.io/javascript/statements/for) har tre delar, det första kallas initiering, där vi sätter loop-variabeln till ett visst värde. I vårt fall har vi valt att ha **i** som loop-variabel. Den andra delen kallas villkorssats, och den testas varje gång loopen körts. Den tredje satsen kallas uppräkningssats, och innebär att loop-variabeln räknas upp med 1 efter varje varv.

Man kan också öka med 10 åt gången:

```php
<script>
console.log("Årtionderna på 2000-talet börjar på åren...");
for (var år = 2000; år < 2100; år += 10)
{
    console.log("årtionden", år);
}
</script>
```

## Loop inuti loop

Om man vill kan man göra göra loopar inuti varje loop. Tex skriv ut årtionden och 5 årtal för varje på följande sätt:

```php
<script>
console.log("Årtionderna på 2000-talet börjar på åren...");
for (var årtionde = 2000; årtionde < 2100; årtionde += 10)
{
    console.log("årtionden", årtionde);
    for (var år = 1; år <= 5; år += 1)
    {
        console.log("år = " + årtionde + år);
    }
}
</script>
```

## Uppgifter

### **Uppgift 1**

Skapa ett program som skriver ut talen **40 till 80** med hjälp av en **for**-loop. Ett tal ska skrivas ut per rad, 40 ska skrivas på första raden och 80 på den sista.

### **Uppgift 2**

Skapa ett program som skriver ut talen **50 till 1** med hjälp av en **for**-loop. Ett tal ska skrivas ut per rad, 50 ska skrivas på första raden och 1 på den sista.

### **Uppgift 3**

Gör ett skript som skriver ut ett **tal** och respektive **tals kvadrat** från **0 till 50**.

### Uppgift 4

Skriv ett skript som frågar efter **två heltal**, det första talet ska vara lägre än det andra. \
Skriv ut konsolen **alla heltal** mellan de två som matats in. Separera med mellanslag.

## Extra uppgifter

### Uppgift 5

Skapa ett program som skriver ut vart **5:e årtal** på 1400-talet med början på **1495 **och sedan nedåt, det vill säga 1495, 1490, 1485 och så vidare ända till **1400**.

### Uppgift 6

Skapa ett program som skriver ut alla tal från 1 till 30 som inte är delbara med 4. Utnyttja ett **continue** någonstans i din loop.\
Ett tal är delbart med 4 om resten vid division med 4 är 0, så ett tal är därmed inte delbart med 4 om resten vid division med 4 blir något annat än 0.

### Uppgift 7

Skapa ett program som skriver ut en ihålig rektangel bestående av X. Användaren ska bestämma rektangelns bredd och höjd. Som exempel så ska en rektangel med bredden 7 och höjden 5 se ut så här:

```
XXXXXXX
X     X
X     X
X     X
XXXXXXX
```

### Uppgift 8

Skapa ett program som skriver ut talen 10, 11, 12 o.s.v. upp till och med 30. Därefter ska programmet skriva ut talen 200, 199, 198 o.s.v. ner till och med 180. Slutligen ska programmet skriva ut talen 1000, 1050, 1100, 1150 o.s.v. upp till och med 1400.

### Uppgift 9

Skapa ett program där användaren ska skriva in ett **heltal**. Efter att hen har gjort det ska programmet fråga om användaren vill skriva in ett heltal till eller inte. Användaren ska fortsätta att få skriva in nya heltal ända tills hen inte vill göra det mer. När användaren inte vill skriva in mer tal ska programmet skriva ut vilket som var det största talet som användaren skrev in.

### Uppgift 10

Det finns en klassisk programmeringsuppgift som kallas _Fizz Buzz_. Uppgiften är denna: Skapa ett program som skriver ut talen 1 till 100 på var sin rad. Men talen som är delbara med 3 ska inte skrivas ut, istället ska ordet **Fizz **skrivas. Talen som är delbara med 5 ska inte heller skrivas ut, skriv istället ut **Buzz**. Tal som är delbara med både 3 och 5 ska ersättas med **FizzBuzz**.

### Uppgift 11

Skapa ett program som ritar ut en rätvinklig triangel där användaren får bestämma triangelns sidlängd. Om användaren till exempel anger sidlängden 5 så ska följande triangel ritas ut:

```
*
**
***
****
*****
```
