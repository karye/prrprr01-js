---
description: Nu vidare till nästa variant på styrstrukturer, nämligen loopar.
---

# Loopar

## Oändliga loopen

![Har datorn hamnat i en oändlig loop?](<../../.gitbook/assets/image (14).png>)

### Oändlig loop

* Upprepa programflödet med en **while**-loop

```php
<body>
    <div class="kontainer">
        <h1>Oändlig loop</h1>
        <button onclick="loopa()">Är du redo?</button>
    </div>
    <script>
        function loopa() {
            // Köra en oändlig loop
            while (true) {
                alert("Busted!");
            }
        }
    </script>
</body>
```

### En enkel spelloop

* Loopen i all evighet
* Men det går att bryta loopen med **break**

```php
<script>
function loopa() {
    var slumptal = "42";

    // Köra en oändlig loop
    while (true) {
        
        // Användaren gissar
        var gissning = prompt("Vad är meningen med livet?");

        // Testa om rätt svar
        if (gissning == slumptal) {
            alert("Great du är så smart!");
            break;
        } else {
            alert("Nope, försök igen!");
        }
    }
}
```

### Loopa 5 gånger

* Med en variabel kan man hålla reda på hur många gånger man loopar

```php
<script>
function loopa() {
    // Slumptal 1, 2, 3, 4 .... 50
    var slumptal = Math.ceil(Math.random() * 50);
    console.log("slumptal", slumptal);

    // Antal chanser
    var chans = 5;

    // Köra en oändlig loop
    while (true) {

        // Avbryt om inga chanser kvar
        if (chans == 0) {
            break;
        }
        
        // Användaren gissar
        var gissning = prompt("Gissa ett tal?");

        // Testa om rätt svar
        if (gissning == slumptal) {
            alert("Great du är så smart!");
            break;
        } else {
            alert("Nope, försök igen!");
        }

        chans--;    // Minska med ett (dekrementera)
        console.log("chans", chans);                
    }
}
```

### Loopa så länge...

* Om man bestämt att bara loopa max 5 gånger

```php
<script>
function loopa() {
    // Slumptal 1, 2, 3, 4 .... 50
    var slumptal = Math.ceil(Math.random() * 50);
    console.log("slumptal", slumptal);

    // Antal varv
    var varv = 0;

    // Loopa 5 varv
    while (varv < 5) {

        // Användaren gissar
        var gissning = prompt("Gissa ett tal?");

        // Testa om rätt svar
        if (gissning == slumptal) {
            alert("Great du är så smart!");
            break;
        } else {
            alert("Nope, försök igen!");
        }

        // Räkna upp varv
        varv++;
        console.log("varv", varv);                
    }
}
```
