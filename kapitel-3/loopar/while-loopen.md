---
description: När man vill upprepa koden
---

# while-loopen

## while-loopen

Den första varianten vi ska titta på är [while-loopen](https://devdocs.io/javascript/statements/while):

```php
<script>
while (true) {
   var svar = prompt("Vad heter det mest spelade spelet?");
   console.log("Ett försök till ", svar);
}
</script>
```

Villkoret används för att upprepa något **tills ett visst villkor** är uppfyllt. Här bryts loopen när villkoret är uppfyllt: 

```php
<script>
var svar = "";
while (svar != "Minecraft") {
   svar = prompt("Vad heter det mest spelade spelet?");
   console.log("Ett försök till ", svar);
}
</script>
```

Motsvarande kod för att upprepa 10 gånger skulle alltså se ut så här:

```php
<script>
var i = 1;
while (i < 11) {
   console.log("Detta är rad", i);
   i++;
}
</script>
```

Inuti loopar kan man också använda [break ](https://devdocs.io/javascript/statements/break)för att omedelbart avbryta körningen av loopen och fortsätta direkt med koden som kommer efter loopens slut. Med [continue ](https://devdocs.io/javascript/statements/continue)kan man fortsätta direkt till nästa varv av loopen.

## Uppgifter

### **Uppgift 1**

Skapa ett skript som frågar användaren, ”**Vilket är Europas folkrikaste land?**”. \
Så länge som användaren svarar fel ska hen få en ny chans att svara på frågan.

### **Uppgift 2**

Gör ett skript som frågar efter ett valfritt **lösenord.**\
Om lösenordet inte är korrekt ska användaren få upp inloggningsrutan igen.

### **Uppgift 3**

Skapa ett skript där användaren får svara på frågan ”**Vad heter den kommersiella datorspelet?**”. Användaren får **maximalt 5** chanser på sig att svara. När användaren svarar rätt så ska programmet skriva ut att rätt svar angavs, därefter ska det avslutas. Utnyttja ett **break** någonstans i din loop.

### Uppgift 4

Skapa ett skript som skriver ut talen 50 till 1 i **loggen **med hjälp av en **while**-loop. Ett tal ska skrivas ut per rad, 50 ska skrivas på första raden och 1 på den sista.

## Extra uppgifter

### Uppgift 5

Skriv ett skript som frågar efter **två heltal**, det första talet ska vara lägre än det andra. \
Skriv ut **alla heltal** mellan de två som matats in. \
Separera med mellanslag.

### Uppgift 6

Skapa ett program där användaren ska skriva in ett **heltal**. Efter att hen har gjort det ska programmet fråga om användaren vill skriva in ett heltal till eller inte. Användaren ska fortsätta att få skriva in nya heltal ända tills hen inte vill göra det mer. När användaren inte vill skriva in mer tal ska programmet skriva ut vilket som var det största talet som användaren skrev in.
