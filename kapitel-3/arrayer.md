# Arrayer

![Ett skåp med 3 lådor](<../.gitbook/assets/image (1).png>)

## Arrayer

### Skapa en array

En [array ](https://devdocs.io/javascript/global_objects/array)är en samling av variabler, tänk er ett helt skåp med kökslådor. Matrisen är då hela skåpet och innehåller flera kökslådor. I varje kökslåda finns ett innehåll dvs ett värde. I de flesta programmeringsspråk så börjar man av någon outgrundlig anledning alltid att räkna från och med **0** i stället för **1**. Detta innebär att den första "lådan" / indexet är **0**, den andra **1** och så vidare. För att skapa den här matrisen i form av en kökslåda i skulle vi göra så här:

```php
<script>
var koksskap = [];
koksskap[0] = "bestick";
koksskap[1] = "servetter";
koksskap[2] = "påsar";
</script>
```

Vi fyller alltså helt enkelt "lådorna" en i taget med innehåll. Vi behöver inte ens ta med siffrorna, utan kan låta JavaScript räkna ut dessa åt oss:

```php
<script>
var koksskap = ["bestick", "servetter", "påsar"];
</script>
```

### Använda arrayens värden

Detta ger alltså samma resultat. För att sedan komma åt innehållet i respektive låda (värdet) så gör vi så här:

```php
<script>
console.log("Översta lådan innehåller " + koksskap[0]);
console.log("Mittenlådan innehåller " + koksskap[1]);
console.log("Understa lådan innehåller " + koksskap[2]);
</script>
```

Man kan i stället för att använda 0, 1, 2 ge strängnamn åt indexen.

```php
<script>
var koksskap = [];

koksskap["topp"] = "bestick";
koksskap["mitten"] = "servetter";
koksskap["botten"] = "påsar";

console.log("Översta lådan innehåller " + koksskap["topp"]);
console.log("Mittenlådan innehåller " + koksskap["mitten"]);
console.log("Understa lådan innehåller " + koksskap["botten"]);
</script>
```

När använder man sig då av matriser? Man skulle ju i princip lika gärna kunna använda sig av variabler var och en för sig. Det finns några olika anledningar. Bland annat kan man på ett enkelt sätt sortera i array. Man kan också enkelt systematiskt gå igenom en array och använda alla värdena. När man använder sig av funktioner kommer ofta returvärdet i form av en matris.

## Arbeta med arrayer

### length

Om man vill veta hur många värden det finns i en array använder man egenskapen [length](https://devdocs.io/javascript/global_objects/array/length):

```php
<script>
var koksskap = ["bestick", "servetter", "påsar"];
console.log("Det finns " + koksskap.length + " värden i arrayen");
</script>
```

### push()

Mha av [push()](https://devdocs.io/javascript/global_objects/array/push) kan man lägga till ett värde i arrayen i slutet på en array:

```php
<script>
var namn = ["Gult", "Blått"];
namn.push("Rött");
</script>
```

Nu har arrayen tre värden.

### sort()

Man kan enkelt sortera värdena i en array med [sort()](https://devdocs.io/javascript/global_objects/array/sort):

```php
<script>
var månader = ['Mars', 'Jan', 'Feb', 'Dec'];
månader.sort();
console.log(månader);
// Utskrift: Array ["Dec", "Feb", "Jan", "Mars"]
</script>
```

## Uppgifter

### Uppgift 1

Skapa ett skript som ber användaren om ett **heltal**.\
Berätta för användaren om någon av siffrorna **3** eller **7** fanns i talet.

### Uppgift 2

Skapa ett skript som innehåller en **array **som ska innehålla namnen på tre svenska städer. \
Skriv in två av namnen i koden (hårdkoda) men låt användaren få skriva in namnet på den tredje staden. Skriv avslutningsvis ut alla stadsnamnen på **samma rad**.

### Uppgift 3

Skapa ett skript som innehåller en **array **med **strängar **där varje sträng är en **mening**. \
Skriv ut varje mening med hjälp av en **loop.**\
****Gör så att varje mening hamnar i ett eget **stycke**.

### Uppgift 4

Skapa ett skript som innehåller en **array **med 10 heltal. \
Gå igenom arrayen med hjälp av en **loop** och hitta det största talet i arrayen.

### Uppgift 5

Skapa ett skript som innehåller en array med minst **5** årtal, minst ett av årtalen ska finnas med **två gånger**. Användaren ska få skriva in ett **årtal **när programmet körs. \
Skriptet ska skriva ut **alla index** som detta årtal finns på i arrayen. \
Om årtalet inte fanns på någon plats så ska programmet skriva "**Årtalet kunde inte hittas**".

### Uppgift 6

Skapa ett skript som innehåller en array med minst **5** heltal. \
Beräkna summan av alla heltal i arrayen med hjälp av en **foreach**-loop.

### Uppgift 7

Skapa ett skript där användaren själv väljer hur många **heltal **hen vill skriva in i en **array**. \
Efter att användaren har skrivit in alla tal ska skriptet välja ett av talen som skrevs in slumpvis och därefter skriva ut detta tal samt vilket index detta tal hade i arrayen. Se funktionen [rand()](https://devdocs.io/php/function.rand).

### Uppgift 8

Skapa ett skript där användaren får skriva in en **mening**. \
Skapa en **array **där varje ord i meningen blir ett element i arrayen. \
Skriv därefter ut varje ord i meningen på en egen rad med radnummer.

### Uppgift 9

Skapa ett skript där användaren ska få skriva in åldern på personerna i ett hyreshus. \
Skriptet ska först fråga användaren om en ålder. \
Därefter ska skriptet fråga om användaren vill skriva in en till ålder och förvänta sig svaret "**j**" eller "**n**". Användaren ska få fortsätta skriva in nya åldrar tills hen svarar "**n**". \
Skriv därefter ut alla åldrar som användaren skrev in.

Skriptet ska tillåta användaren att skriva in upp till 100 åldrar utan att krascha, om användaren har skrivit in 100 åldrar ska programmet skriva ut "**Skriptet har inte plats för fler åldrar**" och därefter skriva ut alla åldrar som har sparats.
