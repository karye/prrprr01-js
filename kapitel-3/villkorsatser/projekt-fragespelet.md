# Projekt - frågespelet

## **Syfte**

* Träna på inmatning och utmatning
* Träna på variabler, typer och typkonvertering
* Träna på if-satser

## Interaktiv berättelse

Använd **if-else**-satser tillsammans med det du lärt dig tidigare för att konstruera en interaktiv berättelse med minst två beslut och tre olika slut.

Kända exempel är [Eliza](https://www.masswerk.at/elizabot/) och [Dungeon](https://play.aidungeon.io).



![](<../../.gitbook/assets/image (13).png>)

## Flödesschema

Börja med att designa din berättelse. Använd ett enkelt beslutsträd, tex något av dessa:\


![](https://docs.google.com/drawings/u/0/d/sCVbah0fMe\_4baeG0KFlTag/image?w=572\&h=171\&rev=1\&ac=1\&parent=14wTbpTkwo_McghrUHrnIT7yZQJ79HvoMWTnrXWkoWLM)

## Instruktioner

* Använd **prompt()** och **alert()**
* Använd **if-else**-satser för att undersöka vilket val spelaren gjort
* Skriv **kommentarer **i koden om vad de olika koderna gör.
* Minst en klasskamrat ska ha testat din berättelse & ha tittat på koden.

## Förbättringar

Om du har tid, fundera över förbättringar, tex:

* Lägg in snygga [emojis](https://www.w3schools.com/charsets/ref_emoji.asp).

### Avancerat

* Gör så att det inte spelar någon roll vilka stora eller små bokstäver man skriver in (t.ex. via || eller genom att använda **toLowerCase()**).
* Gör så att något val har tre alternativ istället för två.
* Använd en loop för att se till så att användaren inte kan gå vidare förrän hen skrivit ett svar (inga tomma svar tex)

### Ännu mer avancerat

* Lägg in varje “rum” i en egen metod, och anropa metoderna från varandra (t.ex. att metod2 anropas inuti metod1).
* Lägg in frågandet i en metod, som ställer frågan och inte avslutas innan spelaren skrivit in ett giltigt svar. Då returneras valet till koden där metoden anropades.
