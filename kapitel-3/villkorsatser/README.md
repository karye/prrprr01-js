---
description: 'Det finns två huvudtyper av styrstrukturer: villkorssatser och loopar.'
---

# if-satser

Hittills har alla program vi skrivit körts igenom rad för rad, utan att vi kunnat styra in programmet på olika vägar beroende på vad användaren har matat in. Det är detta vi har styrstrukturer till. Styrstrukturer har det gemensamt att de kontrollerar om ett uttryck stämmer med ett annat och sedan beroende på resultatet gör ett val. På detta sätt kan man skriva mycket mer flexibla program.

## if-satsen

```php
<script>
if (lösenord == "test123") {
   alert("Du är inloggad");
}
</script>
```

I detta lilla skript använder vi en villkorssats, eller **if**-sats som den också heter. **if**-satsen är sig lik från nästan alla programmeringsspråk och är nödvändig för att man ska anpassa vad som händer efter vad användaren gör. Det som står innanför parenteserna är det s.k. jämförelseuttrycket. I detta fall kontrollerar vi om variabeln **lösenord** har innehållet "test123". Observera att vi använder två likamedtecken! Hade vi bara använt ett likamedtecken hade vi i stället tilldelat variabeln **lösenord** värdet test123.

![](<../../.gitbook/assets/image (4).png>)

Om lösenordet stämmer, körs allting som står innan för krullparanteserna. Om lösenordet inte stämmer, fortsätter programmet efter högerkrullparantesen. Vi kan också välja att ha med ett alternativ som ska inträffa då lösenordet inte stämmer. Då kan det se ut så här:

```php
<script>
if (lösenord == "test123") {
   alert("Du är inloggad");
} else {
   alert("Fel lösenord");
}
</script>
```

I det här fallet kommer vi alltså att få meddelandet "Fel lösenord" om lösenordet inte stämmer.

### Övning 1

```php
<body>
    <div class="kontainer">
        <h1>Inloggning</h1>
        <button onclick="inloggning()">Ange lösenord</button>
    </div>
    <script>
        function inloggning() {
            var lösenord = prompt("Ange ett lösenord");

            // Om lösenordet är rätt!
            if (lösenord == "1234") {
                alert("Bra jobbat!");
            } else {
                document.body.style.cssText = "background: red;";
                alert("Fel lösenord!!! Försök igen!");
            }
        }
    </script>
</body>
```

### Övning 2

```php
<body>
    <div class="kontainer">
        <h1>Får du köpa alkohol</h1>
        <button onclick="alkotest()">Ange ålder</button>
    </div>
    <script>
        function alkotest() {
            var ålder = prompt("Ange din ålder");

            // Om ålder >= 20 då ....
            // Annars skicka till Bolibompas hemsida
            if (ålder >= 20) {
                window.location.href = "https://systembolaget.se";
            } else {
                window.location.href = "https://svtplay.se/genre/bolibompa";
            }
        }
    </script>
</body>
```

Det finns fler jämförelseoperatorer än **==**, även om **==** är den överlägset vanligaste. Vi har bland annat **<** som står för mindre än, **>** som står för större än. Dessa två används endast för tal. **!=** står för inte lika med och kan däremot användas både för tal och strängar. Man kan bygga på **if**-satser med **else if**. På detta sätt kan skriptet ta fler än två olika vägar:

```php
<script>
if (veckodag == "lördag") {
   alert("Det är lördag och jag kan ta det lugnt.";
} else if (veckodag == "söndag") {
   alert("Söndag är vilodag.");
} else {
   alert("Vanlig vardag");
}
</script>
```

### Övning 3

```php
<body>
    <div class="kontainer">
        <h1>Tips på saker att göra</h1>
        <button onclick="tips()">Ange dag</button>
    </div>
    <script>
        function tips() {
            var dag = prompt("Vad är det för dag idag?");

            // Om det är fredag, köp chips på Lidl, annars...
            if (dag == "fredag") {
                window.location.href = "https://estrella.se";
            } else if (dag == "lördag") {
                window.location.href = "https://netflix.com";
            } else if (dag == "söndag") {
                window.location.href = "https://ikea.se";
            }
        }
    </script>
</body>
```

### Övning 4

![](<../../.gitbook/assets/image (12).png>)

### Övning 5

![](<../../.gitbook/assets/image (11).png>)

## Uppgifter

### **Uppgift 1**

Gör ett skript som frågar efter **användarnamn**.\
Om användarnamnet är korrekt ska ett meddelande visas där det står "**Trevligt att se dig igen!**".\
Annars ska användaren få upp meddelande "**Vem är du?**".

### **Uppgift 2**

Skapa ett skript som simulerar detta flödesschema:

![](<../../.gitbook/assets/image (7).png>)

### Uppgift 3

Gör ett skript som fråga efter **ålder**.\
Skriptet svarar med ett meddelande enligt följande tabell:

| Ålder | Åldersgräns som upphör                     |
| ----- | ------------------------------------------ |
| 11    | Se vissa filmer på bio utan vuxet sällskap |
| 13    | Utföra lättare arbete                      |
| 15    | Köra moped                                 |
| 16    | Övningsköra bil                            |

### Uppgift 4

Skapa ett skript som frågar användaren hur många **datorer** hen äger. Skriptet ska sedan skriva ut korrekt i singular eller plural.\
Det innebär att om användaren har en dator skrivs det ut "**Du har 1 dator**". Om användaren har tex 3 datorer skrivs det ut "**Du har 3 datorer**".\
Skriptet får endast ha **en if-sats**.

### Uppgift 5

Skapa ett skript som simulerar detta flödesschema:

![](<../../.gitbook/assets/image (8).png>)

## Extra uppgift

### Uppgift 6

Modifiera **temperaturkonverteringsskriptet**.\
Användaren matar först in:

* **C** för att  konverteringen ska ske från F till C  
*   **F** för att konverteringen ska ske från C till F

    Sedan matar användaren in en temperatur.

Skriptet svarar med den konverterade temperaturen.
