# Logiska operatorer

## Logiska operatorer

Man kan även testa att två olika villkor i samma **if**-sats. Detta kan göras med **&&** (and) eller med **||** (or). Använder man **&&** måste båda villkoren stämma. Med **||** räcker det att minst ett av dem stämmer.

```php
<script>
var veckodag = "lördag";
var vecka = 41;

// Båda villkoren måste stämma
if (veckodag == "lördag" && vecka == 41) {
   alert("Ja, det stämmer");
}

// Det räcker att ett villkor stämmer
if (veckodag == "lördag" || vecka == 20) {
   alert("Söndag är vilodag.");
}
</script>
```

## switch-satsen

Istället för att använda flera **if-else**-satser efter varandra kan man använda **switch**-satsen.

```php
<script>
switch (veckodag) {
    case "lördag":
        alert("Det är lördag och jag kan ta det lugnt.");
        break;
    case "söndag":
        alert("Söndag är vilodag.");
        break;
    default:
        alert("Vanlig vardag");
        break;
}
</script>
```

## Uppgifter

### **Uppgift 1**

Gör ett skript som frågar efter **användarnamn** **och lösenord**.\
Om användarnamnet och lösenordet är korrekt ska ett meddelande visas där det står "**Du är inloggad**".\
Annars ska användaren få upp meddelande "**Fel lösenord**".

### **Uppgift 2**

För att få åka berg-och-dalbana på en nöjespark så måste man vara mellan **1,4 och 1,9** meter lång.\
Skapa ett skript som frågar om användarens **längd**.\
Skriptet svarar om användaren får åka eller inte.

### Uppgift 3

Ett fik har en kampanj där personer äldre än **65** år och personer mellan **12 och 18** år erbjuds att köpa kaffe extra billigt.\
Skriv ett skript som kollar om användaren får köpa kaffe extra billigt.\
Skriptet får endast ha **en if-sats**.

### Uppgift 4

För att få delta i en tävling måste man vara **mellan 16 och 19** år gammal.\
Skapa ett skript som frågar användaren hur gammal hen.\
Skriptet skriver sedan ut om hen får delta i tävlingen och om hen är **för** ung eller om hen är **för gammal**.

### **Uppgift 5**

Gör ett skript som fungerar som en **lånekalkylator**.\
Användaren matar först lånetiden **1, 3 och 5 år**.\
Sedan matar användaren in **lånebeloppet**.\
Slutligen **räntan** i hela procent.

Skriptet ska räkna ut den totala lånekostnaden.\
Räknas ut genom ränta på ränta-principen, årsvis).\
Så för ett tvåårigt lån på 5000 med räntan 4% skulle alltså lånekostnaden bli 5000\*1,04\*1,04 - 5000. Observera att lånet är "amorteringsfritt".

### Uppgift 6

Skapa ett skript som frågar vilket land som vann **fotbolls-VM i USA** för herrar år **1994**.\
Om användaren svarar **Sverige** ska skriptet skriva ut att svaret var **rätt**, annars att svaret var **fel**.\
Det spelar ingen roll om användaren gissar med stora eller små bokstäver.\
Använd [toLowerCase()](https://devdocs.io/javascript/global_objects/string/tolowercase).

### Uppgift 7

På det nationella provet i Svenska 1 så fanns följande **poänggränser**.

| Betyg | Poänggräns |
| ----- | ---------- |
| A     | 55         |
| B     | 45         |
| C     | 35         |
| D     | 25         |
| E     | 15         |

Skapa ett skript som frågar användaren hur många poäng hen fick på provet.\
Skriptet ska svara vilket blev **betyget**.

### Uppgift 8

Skapa ett skript som frågar användaren vilken **plats** hen kom på i senaste idrottsturneringen.\
Skriptet ska sedan berätta om användaren fick **guld**, **silver**, **brons** eller **ingen** medalj.\
Skriptet får endast ha en **switch**-sats,

### Uppgift 9

Ett företag vill anställa gymnasiestudenter som personal.\
Skapa ett skript som frågar användaren om hen har gått ut gymnasiet. Användaren ska uppmanas att svara **j **för **ja** eller **n **för **nej**.\
Därefter ska skriptet fråga hur gammal hen är.

Om hen har gått ut gymnasiet och är under 22 år så ska skriptet säga ”**Vi vill gärna anställa dig**”, annars ska skriptet säga ”**Vi letar tyvärr efter annan personal just nu**”.\
Skriptet får endast ha **en if-sats**.
