# På egen hand

## Träna på programmeringstänk

### Öva i Scratch

* [https://www.kodboken.se/start/skapa-spel/uppgifter-i-scratch](https://www.kodboken.se/start/skapa-spel/uppgifter-i-scratch)

### Från Scratch till Javascript

{% embed url="https://www.youtube.com/watch?v=myQR_caFaGM&list=PL39Sm336N_h9d7UuN2x9NKruc6nQ3ZUnZ" %}

