# Table of contents

* [Introduktion](README.md)
* [Utvecklingsmiljö](utvecklingsmiljoe.md)
* [På egen hand](pa-egen-hand.md)

## Kapitel 1

* [Första koden](kapitel-1/foersta-koden.md)

## Kapitel 2

* [Variabler och datatyper](kapitel-2/variabler.md)
* [Använda text](kapitel-2/anvaenda-text.md)
* [Interaktivitet](kapitel-2/interaktivitet.md)
* [Läsa/skriva i webbsida](kapitel-2/dom/README.md)
  * [Hitta med querySelector()](kapitel-2/dom/queryselector.md)
  * [Labbar](kapitel-2/dom/labbar.md)
  * [Extra uppgifter](kapitel-2/dom/extra-uppgifter.md)

## Kapitel 3

* [if-satser](kapitel-3/villkorsatser/README.md)
  * [Logiska operatorer](kapitel-3/villkorsatser/villkor.md)
  * [Projekt - frågespelet](kapitel-3/villkorsatser/projekt-fragespelet.md)
* [Loopar](kapitel-3/loopar/README.md)
  * [while-loopen](kapitel-3/loopar/while-loopen.md)
  * [for-loopen](kapitel-3/loopar/for-loopen.md)
* [Arrayer](kapitel-3/arrayer.md)

## Kapitel 4

* [Canvas](kapitel-4/canvas/README.md)
  * [Rita rektanglar och linjer](kapitel-4/canvas/rita-med-canvas.md)
  * [Rita cirklar](kapitel-4/canvas/cirklar.md)
  * [Skriva text](kapitel-4/canvas/skriva-text.md)
  * [Upprepa former](kapitel-4/canvas/upprepa-former.md)
